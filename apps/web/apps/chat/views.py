from django.shortcuts import render, redirect

from .models import Chat, ChatRoom
from .forms import JoinForm


def room(request, room_name):
    username = request.POST.get('username', 'Guest') if request.method == "POST" else 'Guest'
    chat_room = ChatRoom.objects.filter(name=room_name).first()
    if not chat_room:
        chat_room = ChatRoom.objects.create(name=room_name)

    if request.method == "POST":
        content = request.POST.get('content')
        Chat.objects.create(
            username=username,
            room=chat_room,
            content=content
        )
        return JsonResponse({"status": "success", "message": "Message sent."})

    chats = Chat.objects.filter(room__name=room_name)

    context = {
        'username': username,
        'room_name': room_name,
        'messages': chats,
    }
    return render(request, 'chat/room.html', context=context)
