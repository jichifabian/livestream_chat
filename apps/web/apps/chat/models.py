from django.db import models


class ChatRoom(models.Model):
    name = models.CharField(max_length=225)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def chat_room_url(self, *args, **kwargs):
        name = self.name
        endpoint = f'/chat/{name}/'
        return endpoint


class Chat(models.Model):
    username = models.CharField(max_length=150)
    room = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
    content = models.TextField(max_length=1000)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content
